import os
from peewee import PostgresqlDatabase, Model

db = PostgresqlDatabase(
    os.environ['POSTGRE_DBNAME'],
    **{
        'port': os.environ['POSTGRE_PORT'],
        'user': os.environ['POSTGRE_USER'],
        'password': os.environ['POSTGRE_PASSWORD']
    }
)

class BaseModel(Model):
    class Meta:
        database = db
