from peewee_moves import DatabaseManager
from database import db

manager = DatabaseManager(db)

def upgrade():
    # manager.create('models')
    manager.upgrade()
    print(manager.status())

def downgrade():
    manager.downgrade()
    print(manager.status())
