### Running the projet
* setups a PostgreSQL database server
* setup a .env file with the db variables: POSTGRE_USER, POSTGRE_PASSWORD, POSTGRE_DBNAME and POSTGRE_PORT

* **run:**
```
$ pipenv shell
$ pipenv install
$ py setup.py
```
