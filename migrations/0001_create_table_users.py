"""
create table users
date created: 2023-04-08 05:33:03.564099
"""


def upgrade(migrator):
    with migrator.create_table('users') as table:
        table.primary_key('id')
        table.char('username', max_length=255, unique=True)
        table.char('name', max_length=255)
        table.char('email', max_length=255)
        table.datetime('birthday')


def downgrade(migrator):
    migrator.drop_table('users')
