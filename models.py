from peewee import CharField, DateTimeField
from database import BaseModel

class User(BaseModel):
    username = CharField(unique=True)
    name = CharField()
    email = CharField()
    birthday = DateTimeField()
    class Meta:
        table_name = 'users'
