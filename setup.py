from datetime import date
from models import User
from migrator import upgrade

upgrade()
user = User.get(User.username=='test')
if not user:
    user = User.create(
        username="test",
        name='Test',
        email='test@test.com',
        birthday=date(1950, 5, 5)
    )

print(user.username)
